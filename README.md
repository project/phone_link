# Phone link

Provide formatter for text and telephone (only Drupal 8) fields which replace phone number with link:

```php
+38 (000) 000-00-00
```

```php
<a href="tel:380000000000">+38 (000) 000-00-00</a>
// OR
<a href="tel:380000000000">Call to +38 (000) 000-00-00</a>
// OR Skype format
<a href="callto:380000000000">Call to +38 (000) 000-00-00</a>
```

## How to use

1. Install the module.
2. Add new text or telephone (only Drupal 8) field.
3. Go to display settings and chose "Phone link" formatter for you textfield.
4. Set title and text on field formatter settings (optional).
5. Save display settings.

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Maintainers

- Kostia Bohach ([_shy](https://www.drupal.org/u/_shy))
- Anton Karpov ([awd studio](https://www.drupal.org/u/awd-studio))

